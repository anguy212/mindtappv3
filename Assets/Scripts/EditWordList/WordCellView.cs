﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EnhancedUI.EnhancedScroller;

//representation of the data in our scene, Its handles how the data is layed out and formatted.
public class WordCellView : EnhancedScrollerCellView
{
    public Color selectedColor;
    public Color unSelectedColor;
    public Image SelectionPanel;
    public bool selected = false;
    public GameObject WordCell;
    
    public Text wordText;
    //optional and allows you to pass data to the view so that it can be displayed.

    void Start()
    {
        var worlCellButtonColors = WordCell.gameObject.GetComponent<Button>().colors;
        worlCellButtonColors.highlightedColor = worlCellButtonColors.normalColor;
        WordCell.GetComponent<Button>().colors = worlCellButtonColors;
    }


    public void SetData(ScrollerData data)
    {
        //links the Animal Name Text GameObject to this view script
        wordText.text = data.Word;
    }

    public void Selection()
    {
        //SelectionPanel.color = selectedColor;
        /**/
        selected = !selected;
       
            if (selected)
            {
                SelectionPanel.color = selectedColor;
                Debug.Log("You pressed " + wordText.text + ". Is selected.");
                WordCell.tag = "Selected";

            }
            else
            {
                SelectionPanel.color = unSelectedColor;
                Debug.Log("You pressed " + wordText.text + ". Is not selected.");
                WordCell.tag = "Cell";


            }
        
        /**/


    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

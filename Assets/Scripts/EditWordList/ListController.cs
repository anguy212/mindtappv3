﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Storage;
//This controller handles the setting up the data for the scroller and provides doome callbacks that the scroller will request
//when it needs information. The controller can handle any type of data, it doesn't even need a list to work. 
//by inheriting from the IEnhanced ScrollerDelegate interface, we are telling ScrollerController taht we need to set up some callbacks for the EnhancedScroller.

public class ListController : MonoBehaviour, IEnhancedScrollerDelegate
{   //our list of data records
    private List<ScrollerData> _data;
    //inspector fields that we will use to link our GameObjects to this class
    public EnhancedScroller myScroller;
    public WordCellView wordCellViewPrefab;

    public List<string> wordList;
    public InputField inputField;
    public Button insertButton, removeButton, BackButton;
    private string input, dataPath, filename;
    public GameObject[] selectedCell;
    private ListCellData listCellData;
    public DatabaseReference reference;
    public StorageReference storage_ref;
    public FirebaseStorage storage;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;

    // public GameObject tempObj;
    //we create our list of ddata and tell the scroller that it should use the ScrollerController as its delegate.
    //by setting this script as the scroller's delegate, we are telling the scscoller that when it needs information
    //about our data or views, it should ask this script. Finally, we reload the data to get it to display.
    

    void Awake()
    {
        // Set this before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        // Get the root reference location of the database.
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        storage = FirebaseStorage.DefaultInstance;
        // Create a storage reference from our storage service
        storage_ref = storage.GetReferenceFromUrl("gs://tappit-44629.appspot.com");

        InitializeFirebase();
    }

    void Start()
    {
        TouchScreenKeyboard.hideInput = false;
        InitializeList();
    }
    private void OnGUI()
    {
        TouchScreenKeyboard.hideInput = false;
    }

    void InitializeFirebase()
    {        
        //Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //auth.StateChanged += AuthStateChanged;
        //AuthStateChanged(this, null);
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    //helper function/delegate callback that the scroller will call to get how many list items we are expecting. 
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }
    //helper function / delegate callback that tells the scroller how larger to make each cell. 
    //ex: 100f = a static 100 pixels for all cells. 
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 100f;
    }

    // returns which cell view prefab the scroller should use to display the cell at the given dataIndex.
    //First we ask the scroller to create the view for us. If the view has already been created and is in the recycled list,
    //The scroller will recycle the view instead of creating a new one. 
    //Next, the cell view has its data set. 
    //This is optional, but in this case it is what drives the view to update its text UI. Finally we return the cell view to the scroller for processing.
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        WordCellView cellView = scroller.GetCellView(wordCellViewPrefab) as WordCellView;
        cellView.SetData(_data[dataIndex]);
        return cellView;
    }

    void InitializeList()
    {
       
        _data = new List<ScrollerData>();

        //string text1 = File.ReadAllText(Application.persistentDataPath + "/words.txt");
        filename = PlayerPrefs.GetString("EditWordList");
        dataPath = Path.Combine(Application.persistentDataPath, filename + ".txt");
        using (StreamReader streamReader = File.OpenText(dataPath))
        {
            string json = streamReader.ReadToEnd();
            listCellData = JsonUtility.FromJson<ListCellData>(json);

        }


        string[] strValues1 = listCellData.words.Split(',');
        //string[] strValues1 = text1.Split(',');
        foreach (string currWord1 in strValues1)
            if (currWord1 != "")
                wordList.Add(currWord1);


        foreach (string item in wordList)
        {
            _data.Add(new ScrollerData() { Word = item });

        }
        myScroller.Delegate = this;
        myScroller.ReloadData();
        //add listeners for each button
        insertButton.onClick.AddListener(AddWordToList);
        removeButton.onClick.AddListener(RemoveFromList);
        BackButton.onClick.AddListener(ReturnList);

    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                //Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                //Debug.Log("Signed in " + user.UserId);
            }
        }
    }

    void AddWordToList()
    {
        input = inputField.text;
        if (input.Length < 13)
        {
            bool repeatWord = false;
            for (int i = 0; i < wordList.Count; i++)
            {
                if (input == wordList[i])
                {
                    repeatWord = true;
                    break;
                }
            }
            if (!repeatWord)
            {
                wordList.Add(input);
                _data.Add(new ScrollerData() { Word = wordList[wordList.Count - 1] });
                myScroller.ReloadData();
            }
        }

    }

    void RemoveFromList()
    {
        //input = inputField.text;
        //wordList.Remove(input);
        //_data.RemoveAll(ScrollerData => ScrollerData.Word == input);
        //myScroller.ReloadData();
        selectedCell = GameObject.FindGameObjectsWithTag("Selected");
        for (int i = 0; i < selectedCell.Length; i++)
        {
            input = selectedCell[i].GetComponent<WordCellView>().wordText.text;
            wordList.Remove(input);
            _data.RemoveAll(ScrollerData => ScrollerData.Word == input);
        }
        myScroller.ReloadData();

    }

   
    public void ReturnList()
    {
        string text1 = "";
        foreach (string word in wordList)
        {
            text1 += word + ",";
        }
        //File.WriteAllText(Application.persistentDataPath + "/words.txt", text1);
        //update information
        listCellData.words = text1;
        listCellData.numWords = GetNumberOfCells(myScroller);
        string json = JsonUtility.ToJson(listCellData);
        using (StreamWriter streamWriter = File.CreateText(dataPath))
        {
            streamWriter.Write(json);
        }

        FirebaseDatabase.DefaultInstance.GetReference("users").Child(auth.CurrentUser.UserId)
            .ValueChanged += (object sender, ValueChangedEventArgs e) =>
            {
                if (e.DatabaseError != null)
                {
                    //Debug.LogError(e.DatabaseError.Message);
                    return;
                }

                if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
                {
                    reference.Child("users").Child(auth.CurrentUser.UserId).Child("Lists").Child(filename).SetRawJsonValueAsync(json);
                }
            };

        //db
        //reference.Child("users").Child(auth.CurrentUser.UserId).Child("Lists").Child(filename).SetRawJsonValueAsync(json);

        SceneManager.LoadSceneAsync(10, LoadSceneMode.Single);
    }

}

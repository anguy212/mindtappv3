﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EditWordList : MonoBehaviour
{
    public InputField inputField;
    public Text listname, wordDisplay;
    public List<string> wordList;
    public Button CreateWordButton;
    private string input;
    private bool repeatWord;
    // Start is called before the first frame update
    void Start()
    {
        CreateWordButton.onClick.AddListener(AddWordToList);
        //wordList.Add("Hello");
        //wordList.Add("THere");
    }

    void AddWordToList() {
        input = inputField.text;
        repeatWord = false;
        for (int i = 0; i < wordList.Count; i++) {
            if (input == wordList[i]) {
                repeatWord = true;
                break;
            }
        }
        if (!repeatWord) {
            wordList.Add(input);
            DisplayWordList();
        }

    }
    private void DisplayWordList() {
        wordDisplay.text = "";
        for (int i = 0; i < wordList.Count; i++) {
            wordDisplay.text += wordList[i] + " ";
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

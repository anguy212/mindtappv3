﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour
{
    public AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        if (!audio.isPlaying) {
            if (PlayerPrefs.HasKey("volume")) {
                audio.volume = PlayerPrefs.GetFloat("volume");
                audio.Play();
            }
            else {
                audio.Play();
            }
        }
        GameObject[] objs = GameObject.FindGameObjectsWithTag("music");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        else {
            DontDestroyOnLoad(this.gameObject);
        }
    }

}

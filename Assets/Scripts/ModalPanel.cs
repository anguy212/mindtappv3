﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ModalPanel : MonoBehaviour
{
    public Button listButton;
    public Button settingsButton;
    public Button logoutButton;
    public Button backButton;
    public GameObject modalPanelObject;
    public GameObject modalPanelDialogue;
    public Button HamburgerButton;

    private static ModalPanel modalPanel;

    // pop up part
    private void OnEnable()
    {
        transform.SetAsLastSibling();
    }

    public static ModalPanel Instance ()
    {
        if (!modalPanel)
        {
            modalPanel = FindObjectOfType(typeof(ModalPanel)) as ModalPanel;
            if (!modalPanel)
                Debug.LogError("There needs to be one active ModalPanel script on a GameObject in your scene.");
        }
        return modalPanel;
    }

    public void Choice (UnityAction listEvent, UnityAction settingsEvent, UnityAction logoutEvent, UnityAction backEvent)
    {
        modalPanelObject.SetActive(true);
        modalPanelDialogue.SetActive(true);
        HamburgerButton.gameObject.SetActive(false);

        listButton.onClick.RemoveAllListeners();
        listButton.onClick.AddListener (listEvent);
        listButton.onClick.AddListener(ClosePanel);

        settingsButton.onClick.RemoveAllListeners();
        settingsButton.onClick.AddListener(settingsEvent);
        settingsButton.onClick.AddListener(ClosePanel);

        logoutButton.onClick.RemoveAllListeners();
        logoutButton.onClick.AddListener(logoutEvent);
        logoutButton.onClick.AddListener(ClosePanel);

        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(backEvent);
        backButton.onClick.AddListener(ClosePanel);
    }

    public void ClosePanel()
    {
        modalPanelObject.SetActive(false);
        HamburgerButton.gameObject.SetActive(true);
    }
}

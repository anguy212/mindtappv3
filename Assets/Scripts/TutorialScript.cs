﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialScript : MonoBehaviour
{
    public GameObject createAcc;
    public GameObject createWordList;
    public GameObject addWords;
    public GameObject addPictures;
    public GameObject finishList;
    public GameObject prevButton;

    private bool s2;
    private bool s3;
    private bool s4;
    private bool s5;
    private bool s6;

    void Start()
    {
        s2 = true;
        s3 = false;
        s4 = false;
        s5 = false;
    }

    public void NextTutorial()
    {
        Debug.Log("Button Pressed");
        if (s2 == true)
        {
            s2 = false;
            s3 = true;
            createWordList.SetActive(true);
            createAcc.SetActive(false);
            prevButton.SetActive(true);
        }

        else if (s3 == true)
        {
            s3 = false;
            s4 = true;
            addWords.SetActive(true);
            createWordList.SetActive(false);
        }

        else if (s4 == true)
        {
            s4 = false;
            s5 = true;
            addPictures.SetActive(true);
            addWords.SetActive(false);
        }

        else if (s5 == true)
        {
            s5 = false;
            s6 = true;
            finishList.SetActive(true);
            addPictures.SetActive(false);
        }

        else if (s6 == true)
        {
            SceneManager.LoadScene("CreateAcc");
        }
    }

    public void PrevTutorial()
    {
        if (s3 == true)
        {
            s2 = true;
            s3 = false;
            createWordList.SetActive(false);
            createAcc.SetActive(true);
            prevButton.SetActive(false);
        }

        else if (s4 == true)
        {
            s3 = true;
            s4 = false;
            addWords.SetActive(false);
            createWordList.SetActive(true);
        }

        else if (s5 == true)
        {
            s4 = true;
            s5 = false;
            addPictures.SetActive(false);
            addWords.SetActive(true);
        }

        else if (s6 == true)
        {
            s5 = true;
            finishList.SetActive(false);
            addPictures.SetActive(true);
        }
    }

}

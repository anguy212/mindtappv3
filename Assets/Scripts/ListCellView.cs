﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EnhancedUI.EnhancedScroller;

//handles how data is layed out and formatted in a cell
public class ListCellView : EnhancedScrollerCellView
{
    public Text listNameText;
    public Text numWordsText;
    //public Text numPicsText;
    public Text highscoreText;
    public Text defaultText;
    public Image listImage;
    //public Button cellClicked;
    //reference to our game object
    //private bool selected = false;
    public bool selected = false; // changed to public so others can check if cell is selected. Only allows 1 selection at a time
    public GameObject Cell;
    public Color SelectedColor;
    public Color unSelectedColor;
    public GameObject displayStats;

    // This variable stores how many lists are selected ATM, only one list can be selected
    public static GameObject selectedCell;

    public void Awake()
    {
        displayStats = GameObject.FindWithTag("Display");
        selectedCell = null;
    }

    // This section was for keeping 1 list as default. Commented out for now.
    //public void Update()
    //{
    //Debug.Log("Update test");
    /*if (PlayerPrefs.HasKey("DefaultList") && listNameText.text != null && defaultText.gameObject != null)
    {
        if (PlayerPrefs.GetString("DefaultList") == listNameText.text)
        {
            defaultText.gameObject.SetActive(true);
        }
        else
        {
            defaultText.gameObject.SetActive(false);
        }
    }*/
    //}

    public void SetData(ListCellData data)
    {
        listNameText.text = data.listName;
        numWordsText.text = "Words: " + data.numWords.ToString();
        //numPicsText.text = "Pictures: " + data.numPics.ToString();
        highscoreText.text = "Highscore: " + data.highScore;
        listImage = data.listImage;
    }
    //called if cell is clicked
    public void Selection()
    {
        selected = !selected;
        //public Image CellImage = Cell.GetComponent<Image>().color;
        if (Cell.tag != "Default")
        {

            if (selected)
            {
                Debug.Log("You clicked: " + listNameText.text);
                Cell.GetComponent<Image>().color = SelectedColor;
                Cell.tag = "Selected";
                numWordsText.gameObject.SetActive(true);
                highscoreText.gameObject.SetActive(true);



                if (selectedCell == null)
                {
                    selectedCell = Cell;
                }
                else if (!GameObject.ReferenceEquals(selectedCell, Cell))
                {
                    Debug.Log("TESTING PREVIOUS: " + selectedCell.GetComponent<ListCellView>().listNameText.text + " " + listNameText.text);
                    selectedCell.GetComponent<Image>().color = unSelectedColor;
                    selectedCell.tag = "Cell";
                    selectedCell.GetComponent<ListCellView>().numWordsText.gameObject.SetActive(false);
                    selectedCell.GetComponent<ListCellView>().highscoreText.gameObject.SetActive(false);
                    selectedCell.GetComponent<ListCellView>().selected = !selectedCell.GetComponent<ListCellView>().selected;

                    selectedCell = Cell;
                }

            }
            else
            {
                Debug.Log(listNameText.text + "Is not selected");
                Cell.GetComponent<Image>().color = unSelectedColor;
                Cell.tag = "Cell";
                numWordsText.gameObject.SetActive(false);
                highscoreText.gameObject.SetActive(false);

                selectedCell = null;
            }
        }
        else
        {
            if (selected)
            {
                Debug.Log("You unselected the default button");
                Cell.GetComponent<Image>().color = unSelectedColor;
            }
            else
            {
                Debug.Log("You selected the default button");
                Cell.GetComponent<Image>().color = SelectedColor;
            }

        }
    }

}

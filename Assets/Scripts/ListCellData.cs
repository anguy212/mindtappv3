﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//holds data for a cell in the scrollable list
//presentation of this data is dealt with in ListCellView class
[System.Serializable]
public class ListCellData 
{
    public string listName;
    public int numPics;
    public int numWords;
    public string highScore;
    public Image listImage;
    //
    public string words;
}

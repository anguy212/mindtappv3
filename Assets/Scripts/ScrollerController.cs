﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Firebase.Storage;

public class ScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<ListCellData> _data;
    public EnhancedScroller myScroller;
    public ListCellView ListCellViewPrefab;
    public string uid = "12345678";
    public DatabaseReference reference;
    public StorageReference storage_ref;
    public FirebaseStorage storage;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;
    public string defaultListName;

    public ListCellData defaultData;
    public ListCellData selectedListData;
    public Text mainListName, mainHighscore;
    public Button setDefault, delete, AddWords, AddPics, newSet;
    //public Image mainListImage;
    //
    public GameObject Canvas;
    public InputField setName;
    private GameObject Defaultlist;
    public GameObject displayStats;

    // Error Handling
    public Image errorBackground;
    public Text errorMessage;
    private IEnumerator eWait;

    void Start()
    {
        InitializeFirebase();

        //PlayerPrefs.DeleteAll();
        _data = new List<ListCellData> { };
        myScroller.Delegate = this;
        myScroller.ReloadData();
        //reference.Child("users").Child(uid).ValueChanged += Handle_ValueChanged;

        //setDefault.onClick.AddListener(SetDefaultClick);
        setDefault.onClick.AddListener(SetDefaultClick2);
        delete.onClick.AddListener(DeleteClick);
        AddWords.onClick.AddListener(AddWordsClick);
        AddPics.onClick.AddListener(AddPicsClick);
        newSet.onClick.AddListener(newSetClick);

        DisplayDefault();
        if (PlayerPrefs.HasKey("DefaultList"))
        {
            defaultListName = PlayerPrefs.GetString("DefaultList");
            //Debug.Log(defaultListName);
        }
        /**/
        string[] setTitles = PlayerPrefs.GetString("ListTitles").Split(',');
        foreach (string set in setTitles)
        {
            if (set != "")
            {
                Debug.Log("filename:" + set);
                //read from the json file
                string dataPath = Path.Combine(Application.persistentDataPath, set + ".txt");
                using (StreamReader streamReader = File.OpenText(dataPath))
                {
                    string json = streamReader.ReadToEnd();
                    ListCellData listCellData = JsonUtility.FromJson<ListCellData>(json);
                    //Debug.Log(newData.listName);
                    AddData(listCellData);
                    //_data.Add(newData);
                }
            }
        }
        myScroller.ReloadData();
        //PlayerPrefs.Save();

        Debug.Log("Default: " + PlayerPrefs.GetString("DefaultList"));


        Canvas.transform.Find("NewSetPanel").gameObject.SetActive(false);

    }

    void InitializeFirebase()
    {
        // Set this before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        // Get the root reference location of the database.
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        storage = FirebaseStorage.DefaultInstance;
        // Create a storage reference from our storage service
        storage_ref = storage.GetReferenceFromUrl("gs://tappit-44629.appspot.com");

        //Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //auth.StateChanged += AuthStateChanged;
        //AuthStateChanged(this, null);
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                //Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                //Debug.Log("Signed in " + user.UserId);
            }
        }
    }

    //TODO: Edit all 4 onCLick functions to do their action in the backend as well, not just locally
    public void DeleteClick()
    {
        //If deleting the default and there are more lists, set a new default
        GameObject SelectedCell = GameObject.FindGameObjectWithTag("Selected");
        string input = SelectedCell.GetComponent<ListCellView>().listNameText.text;
        Debug.Log("Deleting Cell: " + input);
        //remove from ListTitles
        string listTitles = PlayerPrefs.GetString("ListTitles");
        listTitles = listTitles.Replace(input + ",", "");
        Debug.Log("listTitles: " + listTitles);
        PlayerPrefs.SetString("ListTitles", listTitles);
        //delete json file 
        //File.Delete(Application.persistentDataPath + input + ".txt");
        //updatescroller
        _data.RemoveAll(ListCellData => ListCellData.listName == input);
        //reloadscroller
        myScroller.ReloadData();
        File.Delete(Application.persistentDataPath + "/" + input + ".txt");
        if (input == PlayerPrefs.GetString("DefaultList"))
            PlayerPrefs.DeleteKey("DefaultList");
        PlayerPrefs.Save();

        DisplayDefault();

    }

    public void SetDefaultClick2()
    {
        GameObject SelectedCell = GameObject.FindGameObjectWithTag("Selected");

        string input = SelectedCell.GetComponent<ListCellView>().listNameText.text;
        RemovePrevDefault();
        //Display default notification
        SelectedCell.transform.Find("Text").gameObject.SetActive(true);
        PlayerPrefs.SetString("DefaultList", input);
        Defaultlist = SelectedCell;
        Debug.Log(input + " is Default List");
        PlayerPrefs.Save();
        DisplayDefault();


    }


    public void DisplayDefault()
    {
        //Display object
        GameObject displayStats = GameObject.FindWithTag("Display");

        if (string.IsNullOrEmpty(PlayerPrefs.GetString("DefaultList")))
        { //first time app is loaded
            displayStats.GetComponentInChildren<Text>().text = "No Default";
            displayStats.transform.Find("Highscore").gameObject.GetComponent<Text>().text = "Highscore: 0";
        }
        else
        {//get the information from the default list. 
            string newString = PlayerPrefs.GetString("DefaultList");
            string dataPath = Path.Combine(Application.persistentDataPath, newString + ".txt");
            using (StreamReader streamReader = File.OpenText(dataPath))
            {
                string json = streamReader.ReadToEnd();
                ListCellData listCellData = JsonUtility.FromJson<ListCellData>(json);
                //display Default listname, numberofwords,highscore
                displayStats.GetComponentInChildren<Text>().text = "Default: " + listCellData.listName;
                displayStats.transform.Find("Highscore").gameObject.GetComponent<Text>().text = "Highscore: " + listCellData.highScore;
                displayStats.transform.Find("DefaultWords").gameObject.GetComponent<Text>().text = "Words: " + listCellData.numWords;
            }
        }
    }

    public void RemovePrevDefault()
    {
        //turn off the previous default list. 
        if (Defaultlist != null)
            Defaultlist.transform.Find("Text").gameObject.SetActive(false);
    }

    public void AddWordsClick()
    {
        //get selected cell
        GameObject SelectedCell = GameObject.FindGameObjectWithTag("Selected");

        if (SelectedCell == null)
        {
            errorMessage.gameObject.SetActive(true);
            errorBackground.gameObject.SetActive(true);
            errorMessage.text = "Please Select \nA List";
            eWait = waitError();
            StartCoroutine(eWait);
        }
        else
        {
            string input = SelectedCell.GetComponent<ListCellView>().listNameText.text;
            PlayerPrefs.SetString("EditWordList", input);
            //add to playerpref the file name / name of list
            //append to existing json file the words that have been created. This can be down in editwordlist. 


            SceneManager.LoadSceneAsync(5, LoadSceneMode.Single);
        }
    }

    private IEnumerator waitError()
    {
        yield return new WaitForSecondsRealtime(3.0f);
        Debug.Log("end of routine");
        errorMessage.gameObject.SetActive(false);
        errorBackground.gameObject.SetActive(false);
    }

    public void AddPicsClick()
    {
        SceneManager.LoadSceneAsync(4, LoadSceneMode.Single);
    }


    public void Create()
    {
        Canvas.transform.Find("NewSetPanel").gameObject.SetActive(true);
    }

    public void newSetClick()
    {
        if (setName.text != null)
        {
            ListCellData newData = new ListCellData() { listName = setName.text };
            string json = JsonUtility.ToJson(newData);
            string dataPath = Path.Combine(Application.persistentDataPath, setName.text + ".txt");

            using (StreamWriter streamWriter = File.CreateText(dataPath))
            {
                streamWriter.Write(json);
            }

            Debug.Log("Saving as JSON: " + json);
            //appending to our list of Sets.
            string listTitles = PlayerPrefs.GetString("ListTitles");
            listTitles += setName.text + ",";
            Debug.Log("Our filenames: " + listTitles);
            PlayerPrefs.SetString("ListTitles", listTitles);

            AddData(newData);
            myScroller.ReloadData();
            Canvas.transform.Find("NewSetPanel").gameObject.SetActive(false);

            //add to DB
            //string key = reference.Child("users").Child("7jZtzNSk2QfRrjgZFZUfxb0bii93").Push().Key;
            //reference.Child("users").Child("7jZtzNSk2QfRrjgZFZUfxb0bii93").Child(key)
            //reference.Child("users").Child("7jZtzNSk2QfRrjgZFZUfxb0bii93").Child("lists").SetRawJsonValueAsync(json);
            //reference.Child("users").Child("7jZtzNSk2QfRrjgZFZUfxb0bii93").Child("lists").SetRawJsonValueAsync(json);
        }
    }


    public void AddData(ListCellData newData)
    {
        _data.Add(newData);
    }

    public void UpdateDefaultUI()
    {
        mainListName.text = defaultData.listName;
        mainHighscore.text = defaultData.highScore;
    }
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 250f;
    }
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ListCellView cellView = scroller.GetCellView(ListCellViewPrefab) as ListCellView;
        cellView.SetData(_data[dataIndex]);
        return cellView;
    }
    public void Back()
    {
        SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
    }
}
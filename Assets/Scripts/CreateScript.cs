﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreateScript : MonoBehaviour
{
    public int numLists;
    public string listName;
    public InputField input;
    // Start is called before the first frame update
    void Start()
    {
        numLists = PlayerPrefs.GetInt("ListCount");
        //var hi = new InputField.SubmitEvent();
        //hi.AddListener(CreateNew);
        //input.onEndEdit = hi;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //public void CreateNew(string newName)
    //{
    //    listName = newName;
    //    PlayerPrefs.SetInt("ListCount", (numLists + 1));
    //    PlayerPrefs.SetString("ListName" + numLists, listName);

    //    PlayerPrefs.SetString("ListNumWords" + numLists, "0");
    //    PlayerPrefs.SetInt("ListScore" + numLists, 0);
    //    PlayerPrefs.SetInt("ListNumPics" + numLists, 0);

    //    SceneManager.LoadScene("ManageLists");
    //}

    public void CreateClick()
    {
        listName = input.text;
        PlayerPrefs.SetInt("ListCount", (numLists + 1));
        PlayerPrefs.SetString("ListName" + numLists, listName);

        PlayerPrefs.SetString("ListNumWords" + numLists, "0");
        PlayerPrefs.SetInt("ListScore" + numLists, 0);
        PlayerPrefs.SetInt("ListNumPics" + numLists, 0);

        SceneManager.LoadScene("ManageLists");
    }
}


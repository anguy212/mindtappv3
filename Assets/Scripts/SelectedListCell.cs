﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedListCell : MonoBehaviour
{
    public Text listName;
    public Text highscore;
    public Button setDefault;
    public Button delete;
    public Image listImage;

    public void SetData(ListCellData data)
    {
        listName.text = data.listName;
        highscore.text = data.highScore;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Unity.Editor;

public class LoginHandler : MonoBehaviour
{
    public InputField ID_Field;
    public InputField Password_Field;
    public Button Enter, NewUser, GuestLogin;
    public Button Settings, Trophy;
    private string ID;
    private string Password;
    private FirebaseApp app;
    private Firebase.Auth.FirebaseAuth auth;
    bool LoginSuccess = false;
    //bool loginAsGuest = false;
    //float TransitionTime = 3f;
    AudioSource audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        TouchScreenKeyboard.hideInput = true;
        InitializeFirebase();
        ID_Field.text = "ID";
        Password_Field.text = "Password";
        ID = "";
        Password = "";
        Enter.onClick.AddListener(TaskonClick);
        GuestLogin.onClick.AddListener(GuestLoginClick);
        //Enter.onClick.AddListener(SignIn);
        NewUser.onClick.AddListener(NewUserClick);
        Settings.onClick.AddListener(GoToSettings);
        Trophy.onClick.AddListener(GoToHighScores);
        //added
        // audio = GetComponent<AudioSource>();
        // audio.Play();

    }

    private void OnGUI()
    {
        TouchScreenKeyboard.hideInput = true;
    }

    //We dont need an updata 
    void InitializeFirebase() {
        app = FirebaseApp.DefaultInstance;
        //we create a reference to our real-time firebse
        app.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        if (app.Options.DatabaseUrl == null)
        {
            Debug.Log("ERROR Invalid firebase URL");
        }
        //our reference to the firebase authentication api
        auth = Firebase.Auth.FirebaseAuth.GetAuth(app);
    }

    void TaskonClick() {
        //Debug.Log("You have clicked the button!");
        ID = ID_Field.text;
        Password = Password_Field.text;
        Debug.Log(ID+" "+Password);
        auth.SignInWithEmailAndPasswordAsync(ID, Password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {   //changed to debug log so program doesnt crash when the user fails to sign in.
                Debug.Log("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

            // WARNING: Username and Password is not encrypted
            PlayerPrefs.SetString("PersistentLogin", ID);
            PlayerPrefs.SetString("PersistentPassword", Password);
            LoginSuccess = true;
            //MainMenuTransition();
            //StartCoroutine(MainMenuTransition());
        });

    }

    void GuestLoginClick()
    {
        //Debug.Log("You have clicked the button!");
        //loginAsGuest = true;  
        ID = "id@gmail.com";
        Password = "Password";
        Debug.Log(ID + " " + Password);
        auth.SignInWithEmailAndPasswordAsync(ID, Password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {   //changed to debug log so program doesnt crash when the user fails to sign in.
                Debug.Log("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

            // WARNING: Username and Password is not encrypted
            //PlayerPrefs.SetString("PersistentLogin", ID);
            //PlayerPrefs.SetString("PersistentPassword", Password);
            LoginSuccess = true;
            //MainMenuTransition();
            //StartCoroutine(MainMenuTransition());
        });
    }
    
    void NewUserClick()
    {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }
    /*
    public IEnumerator MainMenuTransition()
    {
        while (TransitionTime > 0.0)
        {
            yield return new WaitForSeconds(1.0f);
            TransitionTime--;
        }
        SceneManager.LoadScene("SelectGame");
    }
    */
    void Update()
    {

        TouchScreenKeyboard.hideInput = true;

        if(LoginSuccess)
        {
            SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
        }
        else if (PlayerPrefs.GetString("PersistentLogin") != "" && PlayerPrefs.GetString("PersistentPassword") != "")
        {
            ID = PlayerPrefs.GetString("PersistentLogin");
            Password = PlayerPrefs.GetString("PersistentPassword");
            
            Debug.Log(ID + " " + Password);
            auth.SignInWithEmailAndPasswordAsync(ID, Password).ContinueWith(task => {
                if (task.IsCanceled)
                {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {   //changed to debug log so program doesnt crash when the user fails to sign in.
                    Debug.Log("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

                // WARNING: Username and Password is not encrypted
                PlayerPrefs.SetString("PersistentLogin", ID);
                PlayerPrefs.SetString("PersistentPassword", Password);
                LoginSuccess = true;
                //MainMenuTransition();
                //StartCoroutine(MainMenuTransition());
            });

            SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
        }
        else if (LoginSuccess){
            
            //TransitionTime -= Time.deltaTime;
            //if (TransitionTime < 0)
            //{
                SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
            //}
        }


    }
    void GoToSettings()
    {
        SceneManager.LoadSceneAsync(7, LoadSceneMode.Single);
        PlayerPrefs.SetString("lastloadedscene", SceneManager.GetActiveScene().name);
    }
    void GoToHighScores()
    {
        SceneManager.LoadSceneAsync(11, LoadSceneMode.Single);
    }


}

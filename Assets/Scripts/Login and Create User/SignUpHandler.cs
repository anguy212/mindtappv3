using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Object;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Database;

public class SignUpHandler : MonoBehaviour
{
    public InputField password;
    public InputField confirmPassword;
    public InputField DD;
    public InputField MM;
    public InputField YYYY;
    public InputField Email_Field;
    public Button Create;
    public Text errorMessage;
    private string Email;
    private string Password;
    private string year;
    private string month;
    private string day;
    private string Birthday_firebase;
    private FirebaseApp app;
    private Firebase.Auth.FirebaseAuth auth;
    //float TransitionTime = 5f;
    bool UserCreated = false;
    public string userKey;
    // Start is called before the first frame update
    void Start()
    {
        errorMessage.text = "";
        InitializeFirebase();
        Create.onClick.AddListener(CreateOnClick);
    }

    void InitializeFirebase()
    {
        app = FirebaseApp.DefaultInstance;
        //we create a reference to our real-time firebse
        app.SetEditorDatabaseUrl("https://new-project-e322b.firebaseio.com/");
        if (app.Options.DatabaseUrl == null)
        {
            Debug.LogError("ERROR Invalid firebase URL");
        }
        //our reference to the firebase authentication api
        auth = Firebase.Auth.FirebaseAuth.GetAuth(app);
        //Get the root reference location of the database
        //DatabaseReference reference = app.DefaultInstance.RootReference;
    }


    public void CreateOnClick()
    {
        Email = Email_Field.text;
        Password = password.text;
        day = DD.text;
        month = MM.text;
        year = YYYY.text;
        Birthday_firebase = year + "-" + month + "-" + day;
        try
        {
            System.DateTime SDate = System.DateTime.Parse(Birthday_firebase);
        }
        catch (System.Exception e)
        {
            errorMessage.text = "Error: birthday must be valid";
            return;
        }

        if (Password.Equals(confirmPassword.text))
        {
            auth.CreateUserWithEmailAndPasswordAsync(Email, Password).ContinueWith(task => { //firebase function to make account
                //*********JUST keep task.isfaulted ON TOP**********
                if (task.IsFaulted)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    errorMessage.text = "Error: email must be valid\nPassword must be at least 6 characters long.";
                    return; //PROBLEM ^ line above prints even if email and password are valid
                    // The return is hit when email and password are invalid though so it 
                }
                else if (task.IsCanceled)
                {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    errorMessage.text = "cannot process";
                    return; //This doesnt seem to ever go in this else if statement 
                }
                else if (task.IsCompleted)
                {
                    // Firebase user has been created.
                    Firebase.Auth.FirebaseUser newUser = task.Result;
                    Debug.LogFormat("Firebase user created successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

                    //returning to main menu
                    //MainMenuTransition();
                    CreateUseronDB(newUser.UserId);
                    UserCreated = true;
                    return;
                }
            });
        }
        else
        {
            Debug.Log("ERROR: Confirm password != Password");
            errorMessage.text = "Password does not match";
        }
    }
    //sets the current data of our user. 

    public void Back()
    {
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }


    void CreateUseronDB(string UID)
    {
        //Debug.Log(UID);
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("users");

        //To push to the database,  you DONT use Ilist.
        /*List<string> DefaultList = new List<string>();
        
        DefaultList.Add("Hello");
        DefaultList.Add("GoodBye");
        DefaultList.Add("Yeet");*/
        User user = new User("0", "0", Birthday_firebase);
        string json = JsonUtility.ToJson(user);
        reference.Child(UID).SetRawJsonValueAsync(json);


    }


    public class User
    {
        //class variables must be public to be displayed onto DB.
        /*public string email;
        public string version;

        public List<string> lists;*/
        public string totalScore;
        public string birthday;
        public string PersonalBest;
        public User() { }

        public User(string totalScore, string personalBest, string birthday)
        {
            /*this.email = email;
            this.version = version;

            this.lists = DefaultList;*/
            this.PersonalBest = personalBest;
            this.totalScore = totalScore;
            this.birthday = birthday;
        }
    }

    string temp = "";

    void Update()
    {
        if (UserCreated)
        {
            SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
        }
        if (MM.isFocused && MM.text.Length > 2)
        {
            temp += MM.text[2];
            MM.text = MM.text.Substring(0, 2);
            DD.Select();
            DD.text = temp;
            StartCoroutine(MoveTextEnd_NextFrame(DD));
            temp = "";

        }

        if (DD.isFocused && DD.text.Length > 2)
        {
            temp += DD.text[2];
            DD.text = DD.text.Substring(0, 2);
            YYYY.Select();
            YYYY.text = temp;
            StartCoroutine(MoveTextEnd_NextFrame(YYYY));
            temp = "";
        }
    }

    IEnumerator MoveTextEnd_NextFrame(InputField inputfield)
    {
        yield return 0;
        inputfield.MoveTextEnd(false);
    }
    /*
    void MainMenuTransition() {
        while (TransitionTime > 0)
        {
            TransitionTime -= Time.deltaTime;
        }
        SceneManager.LoadScene("SelectGame");
    }
    */


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SelectGameScript : MonoBehaviour
{
    public Slider volumeSlider;
    public GameObject modalPanelObject1;
    public GameObject modalPanelDialogue1;

    public void Start()
    {
        modalPanelObject1.SetActive(false);
        modalPanelDialogue1.SetActive(false);
}

    // load scenes
    public void GotoMindCatch()
    {
        SceneManager.LoadScene("MainMenuMC");
    }
    public void GotoTappIT()
    {
        SceneManager.LoadScene("MainMenuTI");
    }
    public void GotoMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void GotoQuestions()
    {
        SceneManager.LoadScene("Questions");
    }
    public void GotoManageList()
    {
        SceneManager.LoadScene("ManageLists");
    }
}
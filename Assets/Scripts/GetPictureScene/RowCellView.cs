﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;
using System;

namespace EnhancedScrollerDemos.GridSimulation
{
    /// <summary>
    /// This is the sub cell of the row cell
    /// </summary>
    public class RowCellView : MonoBehaviour
    {
        public GameObject container;
        public Image imageViewFake;
        public Button delete;
        public CameraGalleryScript galleryScript;
        public Data dataGlobal;

        private void Start()
        {
            galleryScript = FindObjectOfType<CameraGalleryScript>();
        }

        /// <summary>
        /// This function just takes the Demo data and displays it
        /// </summary>
        /// <param name="data"></param>
        public void SetData(Data data)
        {
            // this cell was outside the range of the data, so we disable the container.
            // Note: We could have disable the cell gameobject instead of a child container,
            // but that can cause problems if you are trying to get components (disabled objects are ignored).
            container.SetActive(data != null);
            Debug.Log("Set Data Called");
            if (data != null)
            {
                Sprite newSprite = Sprite.Create(data.texture, new Rect(0, 0, data.texture.width, data.texture.height), new Vector2(0.5f, 0.5f));
                imageViewFake.sprite = newSprite;
                imageViewFake.type = Image.Type.Simple;
                imageViewFake.preserveAspect = true;
                dataGlobal = data;
                Debug.Log(dataGlobal.someText);
            }
        }

        public void DeleteClick() {
            galleryScript.RemovePic(dataGlobal.texture);
            }
        }
}
﻿using System.Collections;
using System.Collections.Generic;
using EnhancedScrollerDemos.GridSimulation;
using UnityEngine;

public class TextureTest : MonoBehaviour
{
    public Controller controller;
    public List<Texture2D> picList;
    public Texture2D texture1;
    public Texture2D texture2;
    public Texture2D texture3;
    public Texture2D texture4;
    public Texture2D texture5;
    public Texture2D texture6;

    //alled before the first frame update
    void Start()
    {
        picList = new List<Texture2D>
        {
            texture1,
            texture2,
            texture3,
            texture4,
            texture5,
            texture6
        };
        controller.LoadData(picList);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

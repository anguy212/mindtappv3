﻿using UnityEngine;

namespace EnhancedScrollerDemos.GridSimulation
{
    /// <summary>
    /// Data class to store information
    /// </summary>
    public class Data
    {
        public string someText;
        public Texture2D texture;
    }
}
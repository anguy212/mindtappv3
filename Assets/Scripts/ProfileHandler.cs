﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ProfileHandler : MonoBehaviour
{
    public Button Back;
    // Start is called before the first frame update
    void Start()
    {
        Back.onClick.AddListener(BackButtonTask);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void BackButtonTask() {
        SceneManager.LoadScene("Settings");
    }

}

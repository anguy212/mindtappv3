﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class TestModalPanel : MonoBehaviour
{
    private ModalPanel modalPanel;

    private UnityAction listAction;
    private UnityAction settingsAction;
    private UnityAction logoutAction;
    private UnityAction backAction;

    private void Awake()
    {
        modalPanel = ModalPanel.Instance();

        listAction = new UnityAction(testList);
        settingsAction = new UnityAction(testSettings);
        logoutAction = new UnityAction(testLogout);
        backAction = new UnityAction(testBack);
    }

    public void TestButtons()
    {
        modalPanel.Choice(listAction, settingsAction, logoutAction, backAction);
    }

    void testList()
    {
        SceneManager.LoadScene("MainMenu");
    }

    void testSettings()
    {
        SceneManager.LoadScene("MainMenu");
    }

    void testLogout()
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    void testBack()
    {
        SceneManager.LoadScene("SelectGame");
    }
}

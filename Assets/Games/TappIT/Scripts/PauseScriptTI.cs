﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseScriptTI : MonoBehaviour
{
    public Button PauseButton;
    public Canvas Pause;
    public GameObject Continue;

    public void pauseClicked()
    {
        //pause the game
        Time.timeScale = 0;
        Pause.gameObject.SetActive(true);
        //PauseButton.gameObject.SetActive(false);
    }

    public void ResumeClicked()
    {
        Time.timeScale = 1;
        Pause.gameObject.SetActive(false);
        //PauseButton.gameObject.SetActive(true);
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //need to add score to database
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public void RestartClicked()
    {
        SceneManager.LoadScene("TappIT");
    }

    public void QuitClicked()
    {
        SceneManager.LoadScene("MainMenuTI");
    }

    public void Continue_Game()
    {
        Time.timeScale = 1;
        Continue.SetActive(false);
    }
}

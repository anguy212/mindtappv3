﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

public class MainMenuScriptTI : MonoBehaviour
{
    public Image errorBackground;
    public Text errorMessage;
    private int errorFlag;//flag for error type, used in error message
    private int number;
    private IEnumerator eWait;

    void Start()
    {
        errorFlag = 0;
    }

    // load scenes
    public void GotoInstructions()
    {
        SceneManager.LoadSceneAsync(8, LoadSceneMode.Single);
    }

    public void GotoPlay()
    {   
        List<string> words = new List<string>();
        string newString = PlayerPrefs.GetString("DefaultList");

        if (File.Exists(Application.persistentDataPath + "/" + newString + ".txt") && newString != "default")
        {
            string dataPath = Path.Combine(Application.persistentDataPath, newString + ".txt");
            using (StreamReader streamReader = File.OpenText(dataPath))
            {
                string json = streamReader.ReadToEnd();
                ListCellData listCellData = JsonUtility.FromJson<ListCellData>(json);
                string[] strValues1 = listCellData.words.Split(',');
                foreach (string currWord1 in strValues1)
                    if (currWord1 != "")
                    {
                        words.Add(currWord1);
                    }
            }
        }
        //get the default from inside the game (already stored. Rn is neutral.txt)
        else if (newString == "default")
        {
            string text = File.ReadAllText("Neutral.txt");
            string[] strValues = text.Split(',');
            foreach (string currWord in strValues)
                words.Add(currWord);
        }
        else
            Debug.Log("error loading in words in main menu");

        //check if able to play
        int pictureinmemory = PlayerPrefs.GetInt("PicCount");
        int picChosen = PlayerPrefs.GetInt("numberofpictures");
        int numbofwords = words.Count;

        if(numbofwords == 0)
        {
            Debug.Log("wordlist is empty or it doesn't exist");
            errorFlag = 1;
            errorW();
            number = 6 - picChosen - numbofwords;
        }
        //more pictures chosen than in memory, display error or if theres not enough words
        else if (numbofwords < (6 - picChosen))
        {
            Debug.Log("not enough words");
            errorFlag = 2;
            errorW();
            number = 6 - picChosen - numbofwords;
        }

        else if (picChosen > pictureinmemory)
        {
            Debug.Log("not enough pictures");
            errorFlag = 3;
            errorW();
            number = picChosen - pictureinmemory;
        }
        
        else {
            SceneManager.LoadSceneAsync(6, LoadSceneMode.Single);
        }

    }

    public void GotoHighScore()
    {
        SceneManager.LoadSceneAsync(9, LoadSceneMode.Single);
    }
    public void GotoSettings()
    {
        SceneManager.LoadSceneAsync(7, LoadSceneMode.Single);
        PlayerPrefs.SetString("lastloadedscene", SceneManager.GetActiveScene().name);
    }

    public void GotoManageLists() {
        SceneManager.LoadSceneAsync(10, LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        // SceneManager.LoadScene("TappIT");

        // This commented out section exits the game
        /*#if UNITY_EDITOR
          UnityEditor.EditorApplication.isPlaying = false;
        #else
          Application.Quit();
        #endif*/
        // Logs user out, returns to login screen.
        PlayerPrefs.SetString("PersistentLogin", "");
        PlayerPrefs.SetString("PersistentPassword", "");
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }

    private void errorW()
    {
        Debug.Log("error func");
        errorMessage.gameObject.SetActive(true);
        errorBackground.gameObject.SetActive(true);
        if(errorFlag == 1)
        {
            errorMessage.text = "Wordlist is empty \nor nonexistant";
        }
        else if (errorFlag == 2)
        {
            errorMessage.text = "Please Add More \n Words";
        }
        else if (errorFlag == 3)
        {
            errorMessage.text = "Please Add More \n Pictures";
        }
        eWait = waitError();
        StartCoroutine(eWait);
    }

    private IEnumerator waitError()
    {
        yield return new WaitForSecondsRealtime(3.0f);
        Debug.Log("end of routine");
        errorMessage.gameObject.SetActive(false);
        errorBackground.gameObject.SetActive(false);
    }

}

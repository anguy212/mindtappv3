﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Firebase;
using Firebase.Storage;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using System.Linq;


public class HighScoreScript : MonoBehaviour
{
    public Text DisplayGameHighScore;
    public Text PersonalBest;
    public Text DisplayTotalHighScore;
    DatabaseReference reference;
    public StorageReference storage_ref;
    public FirebaseStorage storage;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;

    List<int> personalBestList = new List<int>();
    List<int> totalScoreList = new List<int>();

    // Start is called before the first frame update
    void Awake()
    {
        
        //highscoreText.text = "";
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");

        reference = FirebaseDatabase.DefaultInstance.RootReference;

        // Get a reference to the storage service, using the default Firebase App
        storage = FirebaseStorage.DefaultInstance;
        // Create a storage reference from our storage service
        storage_ref = storage.GetReferenceFromUrl("gs://tappit-44629.appspot.com");
        /**/

        InitializeFirebase();


        AssignValues();
    }


    void InitializeFirebase()
    {
        //Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
        SetUpDatabase(auth);

        //var ordered = personalBestList.OrderBy(x => x).ToList();

        personalBestList.Sort();


        AssignValues();

    }

    // Track state changes of the auth object.
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
            }
        }
    }

    void SetUpDatabase(Firebase.Auth.FirebaseAuth auth)
    {

        FirebaseDatabase.DefaultInstance.GetReference("users")
           .ValueChanged += (object sender, ValueChangedEventArgs e) =>
           {
               if (e.DatabaseError != null)
               {
                   //Debug.LogError(e.DatabaseError.Message);
                   return;
               }


               if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
               {
                   int i = 0;
                   foreach (KeyValuePair<string,object> info in e.Snapshot.Value as Dictionary<string,object>)
                   {

                       FirebaseDatabase.DefaultInstance.GetReference("users").Child(info.Key)
                        .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
                        {
                            if (e2.DatabaseError != null)
                            {
                                Debug.LogError(e2.DatabaseError.Message);
                                return;
                            }


                            if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
                            {
                                //Debug.Log(e.Snapshot.Child.("totalScore").Value);
                                if(e2.Snapshot.Child("PersonalBest").Value == null)
                                {
                                    Debug.LogError(e2.DatabaseError.Message);
                                    return;
                                }

                                
                                //PersonalBest.text = e2.Snapshot.Child("PersonalBest").Value.ToString();
                                //if(e.Snapshot.HasChild("PersonalBest"))
                                //{
                                    
                                    personalBestList.Add(IntParseFast(e2.Snapshot.Child("PersonalBest").Value.ToString()));
                                    //Debug.Log("USER:::" + info.Key + "Personal Best :::" + personalBestList[i]);
                                    personalBestList.Sort();
                                    personalBestList.Reverse();
                                    i++;
                                //}
                            }
                        };
                   }
               }
           };


        FirebaseDatabase.DefaultInstance.GetReference("users")
           .ValueChanged += (object sender, ValueChangedEventArgs e) =>
           {
               if (e.DatabaseError != null)
               {
                   //Debug.LogError(e.DatabaseError.Message);
                   return;
               }


               if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
               {
                   int i = 0;
                   foreach (KeyValuePair<string, object> info in e.Snapshot.Value as Dictionary<string, object>)
                   {

                       FirebaseDatabase.DefaultInstance.GetReference("users").Child(info.Key)
                        .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
                        {
                            if (e2.DatabaseError != null)
                            {
                                Debug.LogError(e2.DatabaseError.Message);
                                return;
                            }


                            if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
                            {
                                //Debug.Log(e.Snapshot.Child.("totalScore").Value);
                                if (e2.Snapshot.Child("totalScore").Value == null)
                                {
                                    Debug.LogError(e2.DatabaseError.Message);
                                    return;
                                }


                                //PersonalBest.text = e2.Snapshot.Child("PersonalBest").Value.ToString();
                                //if(e.Snapshot.HasChild("PersonalBest"))
                                //{

                                totalScoreList.Add(IntParseFast(e2.Snapshot.Child("totalScore").Value.ToString()));
                                //Debug.Log("USER:::" + info.Key + "totalScore :::" + totalScoreList[i]);
                                totalScoreList.Sort();
                                totalScoreList.Reverse();
                                i++;
                                //}
                            }
                        };
                   }
               }
           };


        FirebaseDatabase.DefaultInstance.GetReference("highscore").ValueChanged += (object sender, ValueChangedEventArgs e) =>
        {
            if (e.DatabaseError != null)
            {
                Debug.LogError(e.DatabaseError.Message);
                return;
            }

            if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
            {
                int i = 0;
                
                foreach (var childSnapshot in e.Snapshot.Children)
                {

                    //Debug.Log("Score: " + childSnapshot.Child("Score").Value.ToString());
                    reference.Child("highscore").Child(e.Snapshot.Key).GetValueAsync().ContinueWith(task =>
                    {
                        if (task.Result == null)
                        {
                            Debug.Log("null!");
                        }
                        else
                        {
                            //int temp_personalBest_int = System.Convert.ToInt32(task.Result.Value.ToString());
                            reference.Child("highscore").Child(childSnapshot.Key).SetValueAsync((personalBestList[i].ToString()));
                            i++;
                        }
                    });
                    
                }
                //DisplayHighScore.text = val;
            }


        };

        FirebaseDatabase.DefaultInstance.GetReference("totalhighscore").ValueChanged += (object sender, ValueChangedEventArgs e) =>
        {
            if (e.DatabaseError != null)
            {
                Debug.LogError(e.DatabaseError.Message);
                return;
            }

            if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
            {
                int i = 0;

                foreach (var childSnapshot in e.Snapshot.Children)
                {

                    //Debug.Log("Score: " + childSnapshot.Child("Score").Value.ToString());
                    reference.Child("totalhighscore").Child(e.Snapshot.Key).GetValueAsync().ContinueWith(task =>
                    {
                        if (task.Result == null)
                        {
                            Debug.Log("null!");
                        }
                        else
                        {
                            //int temp_personalBest_int = System.Convert.ToInt32(task.Result.Value.ToString());
                            reference.Child("totalhighscore").Child(childSnapshot.Key).SetValueAsync((totalScoreList[i].ToString()));
                            i++;
                        }
                    });

                }
                //DisplayHighScore.text = val;
            }


        };


        FirebaseDatabase.DefaultInstance.GetReference("users").Child(auth.CurrentUser.UserId)
           .ValueChanged += (object sender, ValueChangedEventArgs e) =>
           {
               if (e.DatabaseError != null)
               {
                   Debug.LogError(e.DatabaseError.Message);
                   return;
               }

               if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
               {
                    //Debug.Log(e.Snapshot.Child.("totalScore").Value);
                    //Debug.Log(e.Snapshot.Child("PersonalBest").Value);
                   PersonalBest.text = e.Snapshot.Child("PersonalBest").Value.ToString();

                    //foreach (var childSnapshot in e.Snapshot.Children)
                    //{

                    //val += childSnapshot.Child("Score").Value.ToString() + "\n";
                    //PersonalBest.text = childSnapshot.Child("totalScore").Value.ToString() + "\n";
                    //Debug.Log("Score: " + childSnapshot.Child("Score").Value.ToString());

                    //}
                    //DisplayHighScore.text = val;

                }
           };

        /**/
        


    }

    void AssignValues()
    {
        FirebaseDatabase.DefaultInstance.GetReference("highscore").ValueChanged += (object sender, ValueChangedEventArgs e) =>
        {
            if (e.DatabaseError != null)
            {
                Debug.LogError(e.DatabaseError.Message);
                return;
            }
            //Debug.Log("Received values for Leaders.");

            if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
            {
                DisplayGameHighScore.text = "";
                foreach (var childSnapshot in e.Snapshot.Children)
                {

                    //Debug.Log(childSnapshot.Key + "\t" + childSnapshot.Value);
                    //val += childSnapshot.Child("Score").Value.ToString() + "\n";
                    DisplayGameHighScore.text += childSnapshot.Key.ToString() + ":\t" + childSnapshot.Value.ToString() + "\n";

                    //Debug.Log("Score: " + childSnapshot.Child("Score").Value.ToString());

                }
                //DisplayHighScore.text = val;
            }


        };
        FirebaseDatabase.DefaultInstance.GetReference("totalhighscore").ValueChanged += (object sender, ValueChangedEventArgs e) =>
        {
            if (e.DatabaseError != null)
            {
                Debug.LogError(e.DatabaseError.Message);
                return;
            }
            Debug.Log("Received values for Leaders.");

            if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
            {
                DisplayTotalHighScore.text = "";
                foreach (var childSnapshot in e.Snapshot.Children)
                {

                    //Debug.Log(childSnapshot.Key + "\t" + childSnapshot.Value);
                    //val += childSnapshot.Child("Score").Value.ToString() + "\n";
                    DisplayTotalHighScore.text += childSnapshot.Key.ToString() + ":\t" + childSnapshot.Value.ToString() + "\n";

                    //Debug.Log("Score: " + childSnapshot.Child("Score").Value.ToString());

                }
                //DisplayHighScore.text = val;
            }


        };
    }

    public static int IntParseFast(string value)
    {
        int result = 0;
        for (int i = 0; i < value.Length; i++)
        {
            char letter = value[i];
            result = 10 * result + (letter - 48);
        }
        return result;
    }

    void HandleValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            //Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoBack()
    {
        SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
    }

    IEnumerator DataBaseLoading()
    {
        yield return new WaitForSeconds(4f);
    }
    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }
}

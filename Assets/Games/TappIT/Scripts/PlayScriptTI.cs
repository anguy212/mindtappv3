﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Firebase.Database;
using Firebase;
using Firebase.Storage;
using Firebase.Unity.Editor;
using System.Threading.Tasks;
using System.Threading;

//controls the play functions
public class PlayScriptTI : MonoBehaviour
{
    //stats of the player
    [Header("Player Stats")]
    public int score;
    public int lives;
    public float timePlayed;
    private int numRounds;
    private int numCardsShown;
    private int levelScore; // Score to be added based on level. Currently is 5 * level number.
    public int currentHighScore;

    WaitForSeconds shortWait = new WaitForSeconds(1);
    WaitForSeconds longWait = new WaitForSeconds(2);

    [System.Serializable]
    public class winningcard
    {
        public string winningword;
        public int wordorpic;
    }
    public winningcard myCard;

    [Header("Link to UI")]
    //link to player stats in UI
    public Text currentScore;

    public Text initCount; //links the UI text
    public Text showCardText; //links card shown
    public Image showImage;
    public Text TimeLeft;
    public Text whichCard;
    public Image backgroundBlock; //links the background border
    public int countDownDuration; //beginning countdown of 3 seconds before game starts
    public Sprite red, blue, green, wordbackground;
    public Canvas Quit;
    public Image lives1, lives2, lives3;
    public GameObject levelPassedCanvas;

    //show 6 cards section
    public Button topL, topR, L, R, BottomL, BottomR, pauseButton;
    public Image topL1, topR1, L1, R1, BottomL1, BottomR1;
    public Canvas showSix, Pause;

    [Header("Storage(words and pictuers")]
    public List<string> words; //list that contains all the words
    public List<Sprite> defaultPictures; //default pictures
    public List<Sprite> pictures; //if not student version, get from album

    public List<int> shownCards; //the 3 cards that are shown
    public List<string> chosenCards; //all word cards chosen
    public List<int> chosenPicture; //picture(s) chosen
    public List<string> shownOrder; //order in which they appeared (picture word word, word picture word, word word picture)
    public List<int> wordpic;
    public List<int> displaySixOrder;
    public List<int> userScore = new List<int>();

    [Header("Number of pictures and words used")]
    //!!!!!!!!!!!!!!!!
    public int numberofpictures;
    //!!!!!!!!!!!!!!!!!
    public int numberofwords;

    [Header("Winning card/picture")]
    private string WINNINGCARDword; //winning card word
    private int WINNINGCARDpic; //winning picture
    private int picOrwordWin; //is winner picture or word
    private int whichCardNumber; //which card number this was shown eg. was this card shown first? Then whichCardNumber = 1
    private bool hasClick, hasWon;
    private string cardChosen;
    private string correctCardPosition;

    [Header("Chose correct or not")]
    private GameObject actual, pressed;

    [Header("Backend")]
    //Backend stuff
    public string uid = "12345678";
    public string email;
    public DatabaseReference reference;
    public StorageReference storage_ref;
    public FirebaseStorage storage;
    public string defaultList;
    public List<string> wordsBackend;
    public List<string> pictureRefs;
    public bool photoReady = false;
    public GameObject ImageView1;
    public GameObject ImageView2;
    public GameObject ImageView3;
    public GameObject ImageView4;
    public GameObject ImageView5;
    public GameObject ImageView6;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("asdasdasdasd" + numberofpictures);

        InitializeFirebase();
        //TODO: replace uid with getCurrentUser.uid()
        //This triggers the Handle_ValueChanged method
        //reference.Child("users").Child(uid).ValueChanged += Handle_ValueChanged;


        // Set this before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        // Get the root reference location of the database.
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        // Get a reference to the storage service, using the default Firebase App
        storage = FirebaseStorage.DefaultInstance;
        // Create a storage reference from our storage service
        storage_ref = storage.GetReferenceFromUrl("gs://tappit-44629.appspot.com");

        //Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //auth.StateChanged += AuthStateChanged;
        //AuthStateChanged(this, null);
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
        //email = user.Email;


        lives1.gameObject.SetActive(true);
        lives2.gameObject.SetActive(true);
        lives3.gameObject.SetActive(true);

        Time.timeScale = 1;

        initCount.gameObject.SetActive(true);
        showCardText.gameObject.SetActive(false);
        backgroundBlock.gameObject.SetActive(false);
        showImage.gameObject.SetActive(false);

        showSix.gameObject.SetActive(false);
        Pause.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(true);

        chosenCards = new List<string>(); //card number chosen

        //get the number of pictures to be shown from main menu settings
        numberofpictures = PlayerPrefs.GetInt("numberofpictures");
        //numberofpictures = 1; //testing purposes
        numberofwords = 6 - numberofpictures;

        //show beginning score and lives
        currentScore.text = "Score: 0";
        timePlayed = 0.0f;
        lives = 3;
        score = 0;
        numCardsShown = 3;
        levelScore = 5;

        startGame();

    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                //Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                //Debug.Log("Signed in " + user.UserId);
            }
        }
    }

    // Handle initialization of the necessary firebase modules:
    void InitializeFirebase()
    {
        //// Set this before calling into the realtime database.
        //FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        //// Get the root reference location of the database.
        //reference = FirebaseDatabase.DefaultInstance.RootReference;
        //// Get a reference to the storage service, using the default Firebase App
        //storage = FirebaseStorage.DefaultInstance;
        //// Create a storage reference from our storage service
        //storage_ref = storage.GetReferenceFromUrl("gs://tappit-44629.appspot.com");

        //Debug.Log("Setting up Firebase Auth");
        //auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        ////auth.StateChanged += AuthStateChanged;
        ////AuthStateChanged(this, null);
        //user = auth.CurrentUser;
        //email = user.Email;
    }



    public void Update()
    {
        //updates the time played
        timePlayed += Time.deltaTime;
        Application.targetFrameRate = 15;
        if(numCardsShown > 5)
        {
            numCardsShown = 5;
        }
    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //logic to choose which word list to use. either online or default
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //pick (6 - # of picture) cards
    public void getWordList()
    {
        //get list name from playerpref
        words = new List<string>();

        //get custom words
        string newString = PlayerPrefs.GetString("DefaultList");
        //string newString = "customwords";

        if (File.Exists(Application.persistentDataPath + "/" + newString + ".txt") && newString != "default")
        {
            /*
            string path = Application.persistentDataPath + "/" + newString + ".txt";
            string text1 = File.ReadAllText(path);
            string[] strValues1 = text1.Split(',');
            foreach (string currWord1 in strValues1)
                words.Add(currWord1);
            /**/
            string dataPath = Path.Combine(Application.persistentDataPath, newString + ".txt");
            using (StreamReader streamReader = File.OpenText(dataPath))
            {
                string json = streamReader.ReadToEnd();
                ListCellData listCellData = JsonUtility.FromJson<ListCellData>(json);
                string[] strValues1 = listCellData.words.Split(',');
                foreach (string currWord1 in strValues1)
                    if (currWord1 != "")
                    {
                        words.Add(currWord1);
                    }
            }
        }
        //get the default from inside the game (already stored. Rn is neutral.txt)
        else if (newString == "default")
        {
            string text = File.ReadAllText("Neutral.txt");
            string[] strValues = text.Split(',');
            foreach (string currWord in strValues)
                words.Add(currWord);
        }
        //else
            //Debug.Log("error loading in words");

    }

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //logic to pick pictures from online or default
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //pick the random picture
    public void getPictureList()
    {
        
        //get number of pictures
        int numPicsAvailable = PlayerPrefs.GetInt("PicCount");

        //store into list of sprites
        if (numPicsAvailable > 0)
        {
            for (int i = 0; i < numPicsAvailable; i++)
            {
                string photo = "/Photo" + i.ToString();
                if (File.Exists(Application.persistentDataPath + photo))
                {
                    byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + photo);
                    Texture2D texture = new Texture2D(512, 512, TextureFormat.Alpha8, false);
                    texture.LoadImage(bytes);
                    texture.Apply();
                    Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                    pictures.Add(newSprite);
                }
                //else
                    //Debug.Log("cannot access photo from persistentDataPath");
            }
        }
        //else
            //Debug.Log("error with playerprefs get PicCount");
         /*   
        //for testing purposes
        //PlayerPrefs.SetInt("PicCount", 1);
        //get number of pictures
        int numbofPictures = PlayerPrefs.GetInt("PicCount");
        numberofpictures = 1;

        //store into list of sprites
        if (numbofPictures > 0)
        {
            for (int i = 0; i < numbofPictures; i++)
            {
                string photo = "Photo" + i + ".png";
                if (File.Exists(Application.persistentDataPath + "/" + photo))
                {
                    byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + photo);
                    Texture2D texture = new Texture2D(1, 1);
                    texture.LoadImage(bytes);
                    Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                    pictures.Add(newSprite);
                }
                else
                    Debug.Log("cannot access photo from persistentDataPath");
            }
        }
        else
            Debug.Log("error with playerprefs get PicCount");
            */

    }

    //picks the words that will be used at random
    public void pickCards()
    {
        chosenCards.Clear();

        int wordlistLength = words.Count; //number of words in the list
        int randomNumber;

        //add first word
        randomNumber = Random.Range(0, wordlistLength);
        chosenCards.Add(words[randomNumber]);

        //choose words
        while (chosenCards.Count < numberofwords)
        {
            randomNumber = Random.Range(0, wordlistLength);
            //check if already picked
            if (!checkList(words[randomNumber], chosenCards))
                chosenCards.Add(words[randomNumber]);
        }
    }

    //picks the pictures to be used at random
    public void pickPictures()
    {
        int picturelistLength = pictures.Count; //number of pictures
        int randomNumberP;
        chosenPicture = new List<int>();

        //add in first picture
        randomNumberP = Random.Range(0, picturelistLength);
        chosenPicture.Add(randomNumberP);

        //if more than 1 picture used
        while (chosenPicture.Count < numberofpictures)
        {
            randomNumberP = Random.Range(0, picturelistLength);
            if (!checkpictures(randomNumberP, chosenPicture))
                chosenPicture.Add(randomNumberP);
        }
    }

    //game starts
    public void startGame()
    {
        //get wordlist from persistentdatapath
 
        getWordList();

        //get pictures from persistentdatapath
        getPictureList();

        StartCoroutine(initialCountdown());
    }

    //countdown to start the game (3 seconds)
    IEnumerator initialCountdown()
    {
        hasWon = false;
        if (numRounds % 5 == 0)// Jia 
            pauseButton.gameObject.SetActive(false);//Jia 
        else//Jia 
            pauseButton.gameObject.SetActive(true);//Jia
        countDownDuration = 3;
        initCount.gameObject.SetActive(true);

        //reset all the gameobjects to original color, original text and original sprite
        topL.GetComponentInChildren<Text>().color = Color.black;
        topR.GetComponentInChildren<Text>().color = Color.black;
        L.GetComponentInChildren<Text>().color = Color.black;
        R.GetComponentInChildren<Text>().color = Color.black;
        BottomL.GetComponentInChildren<Text>().color = Color.black;
        BottomR.GetComponentInChildren<Text>().color = Color.black;

        topL.GetComponentInChildren<Text>().text = "";
        topR.GetComponentInChildren<Text>().text = "";
        L.GetComponentInChildren<Text>().text = "";
        R.GetComponentInChildren<Text>().text = "";
        BottomL.GetComponentInChildren<Text>().text = "";
        BottomR.GetComponentInChildren<Text>().text = "";

        topL.image.sprite = wordbackground;
        topR.image.sprite = wordbackground;
        L.image.sprite = wordbackground;
        R.image.sprite = wordbackground;
        BottomL.image.sprite = wordbackground;
        BottomR.image.sprite = wordbackground;

        topL1.sprite = null;
        topR1.sprite = null;
        L1.sprite = null;
        R1.sprite = null;
        BottomL1.sprite = null;
        BottomR1.sprite = null;

        //countdown 3,2,1
        while (countDownDuration > 0)
        {
            initCount.text = countDownDuration.ToString();
            countDownDuration--;
            yield return shortWait;
        }

        //pick the cards that will be shown and the correct card
        pickCards();

        //pick a picture
        pickPictures();
        initCount.gameObject.SetActive(false);
        StartCoroutine(showCards());
    }

    //shows the 3 cards
    IEnumerator showCards()
    {
        //not able to pause after cards are being shown
        pauseButton.gameObject.SetActive(false);

        StopCoroutine(initialCountdown());

        backgroundBlock.gameObject.SetActive(true);

        if (shownOrder.Count > 0)
            shownOrder.Clear();

        int randomNumber = 0; //choose random word/picture
        int wordorpic = 0; //choose picture or list. 0 = list, 1 = picture
        List<string> tempWord = new List<string>();
        List<int> tempPic = new List<int>();
        bool nextCard = false;

        while ((tempWord.Count + tempPic.Count < numCardsShown))
        {
            //can't have more than 6 pictures
            if (numberofpictures <= 6)
            {
                //turn off after every card
                if (showCardText.gameObject == true)
                    showCardText.gameObject.SetActive(false);
                if (showImage.gameObject == true)
                    showImage.gameObject.SetActive(false);

                nextCard = false;
                while (!nextCard)
                {
                    wordorpic = Random.Range(0, 2);
                    //choose a random word from wordlist                    
                    if (wordorpic == 0 && tempWord.Count < (numCardsShown - numberofpictures))
                    {
                        randomNumber = Random.Range(0, chosenCards.Count);
                        if (tempWord.Count == 0 || !checkList(chosenCards[randomNumber], tempWord))
                        {
                            showCardText.gameObject.SetActive(true);
                            tempWord.Add(chosenCards[randomNumber]);
                            showCardText.text = chosenCards[randomNumber];
                            shownOrder.Add(chosenCards[randomNumber]);
                            wordpic.Add(0);
                            nextCard = true;
                        }
                    }
                    //choose a random picture from chosenPictures
                    else if (wordorpic == 1 && tempPic.Count < numberofpictures)
                    {
                        randomNumber = Random.Range(0, chosenPicture.Count);
                        if (tempPic.Count == 0 || !checkpictures(randomNumber, tempPic))
                        {
                            showImage.gameObject.SetActive(true);
                            tempPic.Add(randomNumber);
                            showImage.sprite = pictures[chosenPicture[randomNumber]];
                            shownOrder.Add(chosenPicture[randomNumber].ToString());
                            wordpic.Add(1);
                            nextCard = true;
                        }
                    }
                    else
                        nextCard = false;
                }
            }
            
            yield return shortWait;
        }

        StartCoroutine(displayWhichWord());
    }

    //displays which number shown card the player must choose
    IEnumerator displayWhichWord()
    {
        //StopCoroutine(showCards());
        //turn off show 3
        whichCard.gameObject.SetActive(true);
        showCardText.gameObject.SetActive(false);
        showImage.gameObject.SetActive(false);
        backgroundBlock.gameObject.SetActive(false);

        //which of the 3 is the winner
        whichCardNumber = Random.Range(0, numCardsShown);
        myCard.winningword = shownOrder[whichCardNumber];
        myCard.wordorpic = wordpic[whichCardNumber];

        string displayText;
        displayText = AddOrdinal(whichCardNumber);
        

        whichCard.text = "What is the " + displayText + " card?";
        yield return longWait;
        StartCoroutine(displaySix());
    }

    //https://stackoverflow.com/questions/20156/is-there-an-easy-way-to-create-ordinals-in-c
    public static string AddOrdinal(int num)
    {
        num = num + 1;
        switch (num % 100)
        {
            case 11:
            case 12:
            case 13:
                return num + "th";
        }

        switch (num % 10)
        {
            case 1:
                return num + "st";
            case 2:
                return num + "nd";
            case 3:
                return num + "rd";
            default:
                return num + "th";
        }

    }

    //shows all 6 cards
    IEnumerator displaySix()
    {
        StopCoroutine(displayWhichWord());
        //turn off which card
        whichCard.gameObject.SetActive(false);
        //turn show 6 and choose right card on
        showSix.gameObject.SetActive(true);

        hasClick = false;
        //randomize the list chosenCards
        //shuffle();
        int randomNumb = 0;
        //List<int> displaySixOrder = new List<int>();
        displaySixOrder.Clear();
        int currentWords = 0;
        int currPic = 0;

        while (displaySixOrder.Count < 6)
        {
            randomNumb = Random.Range(0, 2);
            if (randomNumb == 0 && currentWords < numberofwords)
            {
                displaySixOrder.Add(randomNumb);
                currentWords++;
            }
            else if (randomNumb == 1 && currPic < numberofpictures)
            {
                displaySixOrder.Add(randomNumb);
                currPic++;
            }
            //else
               // Debug.Log("Next word");
        }

        int whichNumbOn = 0;
        while (displaySixOrder.Count > 0)
        {
            if (displaySixOrder[0] == 0)
            {
                if (topL.GetComponentInChildren<Text>().text == "" && whichNumbOn == 0)
                    topL.GetComponentInChildren<Text>().text = chosenCards[0];
                else if (topR.GetComponentInChildren<Text>().text == "" && whichNumbOn == 1)
                    topR.GetComponentInChildren<Text>().text = chosenCards[0];
                else if (L.GetComponentInChildren<Text>().text == "" && whichNumbOn == 2)
                    L.GetComponentInChildren<Text>().text = chosenCards[0];
                else if (R.GetComponentInChildren<Text>().text == "" && whichNumbOn == 3)
                    R.GetComponentInChildren<Text>().text = chosenCards[0];
                else if (BottomL.GetComponentInChildren<Text>().text == "" && whichNumbOn == 4)
                    BottomL.GetComponentInChildren<Text>().text = chosenCards[0];
                else if (BottomR.GetComponentInChildren<Text>().text == "" && whichNumbOn == 5)
                    BottomR.GetComponentInChildren<Text>().text = chosenCards[0];
                //else
                    //Debug.Log("display word error");

                whichNumbOn++;
                chosenCards.RemoveAt(0);
            }
            else if (displaySixOrder[0] == 1)
            {
                if (topL.GetComponentInChildren<Text>().text == "" && whichNumbOn == 0)
                {
                    topL.image.sprite = pictures[chosenPicture[0]];
                    topL.GetComponentInChildren<Text>().text = chosenPicture[0].ToString();
                    topL.GetComponentInChildren<Text>().color = Color.clear;
                }
                else if (topR.GetComponentInChildren<Text>().text == "" && whichNumbOn == 1)
                {
                    topR.image.sprite = pictures[chosenPicture[0]];
                    topR.GetComponentInChildren<Text>().text = chosenPicture[0].ToString();
                    topR.GetComponentInChildren<Text>().color = Color.clear;
                }
                else if (L.GetComponentInChildren<Text>().text == "" && whichNumbOn == 2)
                {
                    L.image.sprite = pictures[chosenPicture[0]];
                    L.GetComponentInChildren<Text>().text = chosenPicture[0].ToString();
                    L.GetComponentInChildren<Text>().color = Color.clear;
                }
                else if (R.GetComponentInChildren<Text>().text == "" && whichNumbOn == 3)
                {
                    R.image.sprite = pictures[chosenPicture[0]];
                    R.GetComponentInChildren<Text>().text = chosenPicture[0].ToString();
                    R.GetComponentInChildren<Text>().color = Color.clear;
                }
                else if (BottomL.GetComponentInChildren<Text>().text == "" && whichNumbOn == 4)
                {
                    BottomL.image.sprite = pictures[chosenPicture[0]];
                    BottomL.GetComponentInChildren<Text>().text = chosenPicture[0].ToString();
                    BottomL.GetComponentInChildren<Text>().color = Color.clear;
                }
                else if (BottomR.GetComponentInChildren<Text>().text == "" && whichNumbOn == 5)
                {
                    BottomR.image.sprite = pictures[chosenPicture[0]];
                    BottomR.GetComponentInChildren<Text>().text = chosenPicture[0].ToString();
                    BottomR.GetComponentInChildren<Text>().color = Color.clear;
                }
                //else
                    //Debug.Log("display picture error");

                whichNumbOn++;
                chosenPicture.RemoveAt(0);
            }
            //else
                //Debug.Log("display 6 error");

            displaySixOrder.RemoveAt(0);
        }

        //find the correct card position
        if (topL.GetComponentInChildren<Text>().text == myCard.winningword)
            correctCardPosition = "TopLeft";
        else if (topR.GetComponentInChildren<Text>().text == myCard.winningword)
            correctCardPosition = "TopRight";
        else if (L.GetComponentInChildren<Text>().text == myCard.winningword)
            correctCardPosition = "Left";
        else if (R.GetComponentInChildren<Text>().text == myCard.winningword)
            correctCardPosition = "Right";
        else if (BottomL.GetComponentInChildren<Text>().text == myCard.winningword)
            correctCardPosition = "BottomLeft";
        else if (BottomR.GetComponentInChildren<Text>().text == myCard.winningword)
            correctCardPosition = "BottomRight";
        //else
            //Debug.Log("error finding correct position of winning card");

        //how many seconds they have left
        int j = 4; //5 seconds to choose card

        while (j > 0 && !hasClick)
        {
            TimeLeft.text = j.ToString();
            //check if any of the 6 buttons have been pressed. No other way.
            //way to check if a button is pressed, but not able to know which one
            topL.onClick.AddListener(delegate { checkifWin(topL.GetComponentInChildren<Text>().text.ToString(), "TopLeft"); });
            topR.onClick.AddListener(delegate { checkifWin(topR.GetComponentInChildren<Text>().text.ToString(), "TopRight"); });
            L.onClick.AddListener(delegate { checkifWin(L.GetComponentInChildren<Text>().text.ToString(), "Left"); });
            R.onClick.AddListener(delegate { checkifWin(R.GetComponentInChildren<Text>().text.ToString(), "Right"); });
            BottomL.onClick.AddListener(delegate { checkifWin(BottomL.GetComponentInChildren<Text>().text.ToString(), "BottomLeft"); });
            BottomR.onClick.AddListener(delegate { checkifWin(BottomR.GetComponentInChildren<Text>().text.ToString(), "BottomRight"); });

            j--;
            yield return shortWait;
        }

        StartCoroutine(didWin());
    }

    //lights up green if correct, red if wrong
    IEnumerator didWin()
    {
        StopCoroutine(displaySix());

        //able to pause the game
        //pauseButton.gameObject.SetActive(true);

        cardChosen = cardChosen + "1";
        correctCardPosition = correctCardPosition + "1";

        //chose correct word
        if (hasWon)
        {
            TimeLeft.text = "Correct!";
            pressed = GameObject.Find(cardChosen);
            pressed.GetComponent<Image>().sprite = green;
        }
        //chose incorrect word
        else if (hasClick)
        {
            TimeLeft.text = "Incorrect!";
            pressed = GameObject.Find(cardChosen);
            pressed.GetComponent<Image>().sprite = red;

            actual = GameObject.Find(correctCardPosition);
            actual.GetComponent<Image>().sprite = green;
        }
        //didn't chose at all
        else
        {
            TimeLeft.text = "Incorrect!";
            actual = GameObject.Find(correctCardPosition);
            actual.GetComponent<Image>().sprite = green;
        }

        updateStats();
        yield return longWait;

        pressed = null;
        actual = null;
        nextRound();

       
    }

    public void nextRound()
    {
        numRounds++;
        StopCoroutine(didWin());
        showSix.gameObject.SetActive(false);
       

        //GAMEOVER
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //ADD STATS TO DATABASE
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        if (lives == 0)
        {
            //Game over screen and pause the game
            Quit.gameObject.SetActive(true);
            Time.timeScale = 0;

            //user authentication is now included in this script under global variable user.
            //instead of using uid, you could use user.Email
            DatabaseReference highscoreBackend = reference.Child("users").Child(auth.CurrentUser.UserId);
            string key = highscoreBackend.Push().Key;
            //string date = DateTime.UtcNow.ToShortDateString();

            Dictionary<string, object> userEmail = new Dictionary<string, object>();
            Dictionary<string, object> scoreUpdate = new Dictionary<string, object>();

            scoreUpdate["Score"] = score.ToString();
            scoreUpdate["Time Played"] = timePlayed;
            //scoreUpdate["Time Stamp"] = ServerValue.Timestamp;
            //scoreUpdate["DateTime"] = System.DateTime .UtcNow.ToString();
            //scoreUpdate["string"] = System.DateTime.UtcNow.ToShortDateString() + System.DateTime.UtcNow.ToLongTimeString();
            //scoreUpdate["Year"] = System.DateTime.UtcNow.Year.ToString();
            string date = System.DateTime.UtcNow.ToLongDateString() + " " + System.DateTime.UtcNow.ToLongTimeString();
            userEmail[date] = scoreUpdate;

            highscoreBackend.UpdateChildrenAsync(userEmail);
            highscoreBackend.Child("totalScore").GetValueAsync().ContinueWith(task => {
                if (task.Result == null)
                {
                    //Debug.Log("null!");
                }
                else
                {
                    int temp_hightScore_int = IntParseFast(task.Result.Value.ToString());
                    highscoreBackend.Child("totalScore").SetValueAsync((score + temp_hightScore_int).ToString());
                }
            });
            
            highscoreBackend.Child(auth.CurrentUser.UserId).Child("PersonalBest") 
            .GetValueAsync().ContinueWith(task => { 
                if (task.IsFaulted) {
                    // Handle the error...
                }
                else if (task.IsCompleted) {
                    DataSnapshot snapshot = task.Result; // snapshot is the PersonalBest Branch of the user playing
                    // Do something with snapshot...
                    int temp = IntParseFast(snapshot.Value.ToString());  //turn the value of the PersonalBest Branch to integer
                    if(score > temp) //if our score is higher than the integer, update Personal best to score
                    {
                        highscoreBackend.Child("PersonalBest").SetValueAsync(score.ToString());
                    }
                }
            });
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Freelancers verion of PersonalBest may work better!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // FirebaseDatabase.DefaultInstance.GetReference("users").Child(auth.CurrentUser.UserId)
            // .ValueChanged += (object sender, ValueChangedEventArgs e) =>
            // {
            //     if (e.DatabaseError != null)
            //     {
            //         //Debug.LogError(e.DatabaseError.Message);
            //         return;
            //     }

            //     if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
            //     { 
            //         foreach (KeyValuePair<string, object> info in e.Snapshot.Value as Dictionary<string,object>)
            //         {
            //             //Debug.Log("Sciore:" + e.Snapshot.Child("Score").Value.ToString());
            //             var dictionary = info.Value;
            //             FirebaseDatabase.DefaultInstance.GetReference("users").Child(auth.CurrentUser.UserId).Child(info.Key)
            //             .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
            //             {
            //                 if (e2.DatabaseError != null)
            //                 {
            //                     Debug.LogError(e2.DatabaseError.Message);

            //                     return;
            //                 }

            //                     userScore.Add(IntParseFast(score));
            //                     currentHighScore = FindMax(userScore);
            //                     highscoreBackend.Child("PersonalBest").GetValueAsync().ContinueWith(task =>
            //                         {
            //                             if (task.Result == null)
            //                             {
            //                                 //Debug.Log("null!");
            //                             }
            //                             else
            //                             {
            //                                 //int temp_personalBest_int = System.Convert.ToInt32(task.Result.Value.ToString());
            //                                 highscoreBackend.Child("PersonalBest").SetValueAsync((currentHighScore).ToString());
            //                             }
            //                         });
            //                 }

            //             };
            //         }
            //     }

            // };             if (e2.Snapshot != null)
            //                 {
            //                     var dictionary2 = (Dictionary<string, object>)e2.Snapshot.Value;
            //                     string score = dictionary2["Score"].ToString();
            //    
        }

        if (numRounds % 5 == 0)
        {
            Time.timeScale = 0;
            //Debug.Log("Added Card");
            numCardsShown++;
            if(lives != 0)
            {
                levelPassedCanvas.SetActive(true);
            }
            levelScore += 5;
        }
        StartCoroutine(initialCountdown());
    }

    //list Operation Methods
    public static int IntParseFast(string value)
    {
        int result = 0;
        for (int i = 0; i < value.Length; i++)
        {
            char letter = value[i];
            result = 10 * result + (letter - 48);
        }
        return result;
    }

    public int FindMax(List<int> list)
    {
        if (list.Count == 0)
        {
            //Debug.Log("Empty list");
        }
        int max = int.MinValue;
        foreach (int type in list)
        {
            if (type > max)
            {
                max = type;
            }
        }
        return max;
    }

    //update the stats
    public void updateStats()
    {
        if (hasWon)
        {
            score += levelScore;
            currentScore.text = "Score: " + score;
        }
        else
        {
            lives--;
            if (lives == 2)
                lives3.gameObject.SetActive(false);
            else if (lives == 1)
                lives2.gameObject.SetActive(false);
            else if (lives == 0)
                lives1.gameObject.SetActive(false);
            //else
                //Debug.Log("lives error");
        }
    }

    private void disableButtons()
    {

    }

    //checks the word against the list to see if any matches
    public bool checkList(string word, List<string> list)
    {
        bool hasSeen = false;
        for (int a = 0; a < list.Count; a++)
        {
            if (word == list[a])
                hasSeen = true;
        }
        return hasSeen;
    }

    //check if pictures overlap
    public bool checkpictures(int number, List<int> pictures)
    {
        bool hasSeen = false;
        for (int b = 0; b < pictures.Count; b++)
        {
            if (number == pictures[b])
                hasSeen = true;
        }
        return hasSeen;
    }

    //check if the card they chose is correct
    public void checkifWin(string text, string whereCard)
    {
        cardChosen = whereCard;
        hasClick = true;
        //check winning card
        if (text == myCard.winningword)
            hasWon = true;
    }

    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
    }

    ////Obtain the user's default list name
    //void Handle_ValueChanged(object sender, ValueChangedEventArgs e)
    //{
    //    if (e.DatabaseError != null)
    //    {
    //        Debug.LogError(e.DatabaseError.Message);
    //        return;
    //    }
    //    DataSnapshot snapshot = e.Snapshot;
    //    foreach (var item in snapshot.Children)
    //    {
    //        if (item.Key.Equals("defaultList"))
    //        {
    //            //triggers Handle_ValueChanged1 method
    //            //Debug.Log(item.Value.ToString());
    //            defaultList = item.Value.ToString();
    //            reference.Child("lists").Child(item.Value.ToString()).ValueChanged += Handle_ValueChanged1;
    //        }
    //    }
    //}

    //Puts the default list words into words list and pictureRefs into PictureRefs list
    //void Handle_ValueChanged1(object sender, ValueChangedEventArgs e)
    //{
    //    if (e.DatabaseError != null)
    //    {
    //        Debug.LogError(e.DatabaseError.Message);
    //        return;
    //    }
    //    DataSnapshot snapshot = e.Snapshot;
    //    foreach (var item in snapshot.Children)
    //    {

    //       // Debug.Log("one");
    //            if (item.Key.Equals("words"))
    //            {
    //                wordsBackend = new List<string>();
    //                foreach (var word in item.Children)
    //                {
    //                   // Debug.Log(word.Value.ToString());
    //                    wordsBackend.Add(word.Value.ToString());
    //                }
    //            }
    //            else if (item.Key.Equals("pictures"))
    //            {
    //                pictureRefs = new List<string>();
    //                foreach (var pic in item.Children)
    //                {
    //                   // Debug.Log(pic.Value.ToString());
    //                    pictureRefs.Add(pic.Value.ToString());
    //                }
    //            }
    //    }
    //}
    ////gets the file of a photo from a single picture ref and stores it locally
    //public void GetPic(PlayScriptTI instance, string picRef)
    //{
    //    // Create local filesystem URL
    //    string local_url = "file:///local/images/" + picRef;

    //    // Download to the local filesystem
    //    StorageReference storagePhoto = storage.GetReferenceFromUrl(picRef);
    //    storagePhoto.GetFileAsync(local_url).ContinueWith(task => {
    //        if (!task.IsFaulted && !task.IsCanceled)
    //        {
    //            Debug.Log("File downloaded.");
    //        }
    //    });
    //}
    ////Populates a Gameobject imageview with a photo file as a texture
    //public void LoadPhoto(string filePath, GameObject imageView)
    //{
    //    Texture2D tex = null;
    //    byte[] fileData;

    //    if (File.Exists(filePath))
    //    {
    //        fileData = File.ReadAllBytes(filePath);
    //        tex = new Texture2D(2, 2);
    //        tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
    //    }
    //    imageView.GetComponent<RawImage>().texture = tex;
    //}
}

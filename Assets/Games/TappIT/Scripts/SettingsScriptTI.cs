﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsScriptTI : MonoBehaviour
{
    [SerializeField]
    private string formatText = "{0}";

    //public Button backButton;

    public TextMeshProUGUI tmproText;
    public Slider volumeSlider;
    public Slider pictureSlider;
    public AudioSource audiotemp;
    
    public void Start()
    {
        GameObject theaudio = GameObject.FindGameObjectWithTag("music");
        //MusicScript musicscript = theaudio.GetComponent<MusicScript>();
        audiotemp = theaudio.gameObject.GetComponent<AudioSource>();

        pictureSlider.minValue = 0;
        pictureSlider.maxValue = 6;
        pictureSlider.wholeNumbers = true;

        pictureSlider.value = PlayerPrefs.GetInt("numberofpictures");
        volumeSlider.value = PlayerPrefs.GetFloat("volume");
    }

    void Update() {
        if(audiotemp.volume != volumeSlider.value) {
            audiotemp.volume = volumeSlider.value;
            PlayerPrefs.SetFloat("volume", volumeSlider.value);
        }
    }

    public void backButton()
    {
        PlayerPrefs.SetInt("numberofpictures", (int)pictureSlider.value);
        string scene = PlayerPrefs.GetString("lastloadedscene");
        SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);
    }

}
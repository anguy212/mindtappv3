﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Firebase;
using Firebase.Storage;
using Firebase.Database;
using Firebase.Unity.Editor;

public class GlobalHSScript : MonoBehaviour
{
    // public Button b;
    public Text GlobalHSText;
    // Start is called before the first frame update
    void Start()
    {
        InitializeFirebase(); //initialize firebase to connect to root
        GlobalBest(); //function will update GlobalHSText 
    }
    void InitializeFirebase() {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://tappit-44629.firebaseio.com/");
        // Get the root reference location of the database.
        // reference = FirebaseDatabase.DefaultInstance.RootReference;
    }
    void GlobalBest()
    {
        
        FirebaseDatabase.DefaultInstance.GetReference("highscore").GetValueAsync().ContinueWith(task =>
        {
            if(task.IsFaulted)
            {
                GlobalHSText.text = "could not get Personal best";
            }
            else if(task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result; //snapshot contains highscore whole tree connected to highscore as root
                //code below queries snapshot from highscore root to get children at 1, 2, 3, 4, 5 and concatonate 
                //to make it display info for GlobalHSText
                GlobalHSText.text ="First: " + snapshot.Child("1").Value.ToString() + "\n" + "Second: " + snapshot.Child("2").Value.ToString()
                 + "\n" + "Third: " + snapshot.Child("3").Value.ToString() + "\n" + "Fourth: " + snapshot.Child("4").Value.ToString() + "\n" + "Fifth: " + snapshot.Child("5").Value.ToString();
            
            }
            
        });
    }

    public void Back() {
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single); //back is clicked, go to login screen
    }
}

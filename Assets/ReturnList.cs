﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Storage;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using System.Linq;

public class ReturnList : MonoBehaviour
{
    DatabaseReference reference;
    public StorageReference storage_ref;
    public FirebaseStorage storage;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;

    private void Awake()
    {
        InitializeFirebase();
    }

    void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
            }
        }
    }


    public void DatabaseAccess(Firebase.Auth.FirebaseAuth auth)
    {
        FirebaseDatabase.DefaultInstance.GetReference("users").Child(auth.CurrentUser.UserId)
            .ValueChanged += (object sender, ValueChangedEventArgs e) =>
            {
                if (e.DatabaseError != null)
                {
                    //Debug.LogError(e.DatabaseError.Message);
                    return;
                }

                if (e.Snapshot != null && e.Snapshot.ChildrenCount > 0)
                {
                    //reference.Child("users").Child(auth.CurrentUser.UserId).Child("Lists").Child(filename).SetRawJsonValueAsync(json);
                }
            };
    }

    public void ReturnL()
    {
        //string text1 = "";
        //foreach (string word in wordList)
        //{
        //text1 += word + ",";
        //}
        //File.WriteAllText(Application.persistentDataPath + "/words.txt", text1);
        //update information
        //listCellData.words = text1;
        //listCellData.numWords = GetNumberOfCells(myScroller);
        //string json = JsonUtility.ToJson(listCellData);
        //using (StreamWriter streamWriter = File.CreateText(dataPath))
        {
            //streamWriter.Write(json);
            //}

            DatabaseAccess(auth);

            //db
            //reference.Child("users").Child(auth.CurrentUser.UserId).Child("Lists").Child(filename).SetRawJsonValueAsync(json);

            //SceneManager.LoadScene("ManageLists");
        }


        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
